package br.ucsal.bes20201.testequalidade.locadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.builder.LocacaoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	
	@Test
	public void testarCalculoValorTotalLocacao2Veiculos3Dias1ano() {
		
		LocacaoBuilder locacaoBuilder = LocacaoBuilder.umaLocacao();
		
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculo().ano(2019).situacao(SituacaoVeiculoEnum.DISPONIVEL);;
		
		Veiculo veiculo1 = veiculoBuilder.placa("QMN4089").ano(2019).diaria(150.00).build();
		Veiculo veiculo2 = veiculoBuilder.placa("PLM9900").ano(2019).diaria(100.00).build();
		
		Locacao locacao1 = locacaoBuilder.umaLocacao().comVeiculos(veiculo1, veiculo2).quantidadeDiasLocacao(3).build();
		
		Double saidaEsperada = 750.00;
		Double saidaAtual = LocacaoBO.calcularValorTotalLocacao(locacao1.getVeiculos(), locacao1.getQuantidadeDiasLocacao());

		Assertions.assertEquals(saidaEsperada, saidaAtual);
		
	}
	
	@Test
	public void testarCalculoValorTotalLocacao3Veiculos3Dias8anos() {
		
		LocacaoBuilder locacaoBuilder = LocacaoBuilder.umaLocacao();
		
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculo().ano(2012).situacao(SituacaoVeiculoEnum.DISPONIVEL);;
		
		Veiculo veiculo1 = veiculoBuilder.placa("TUN0009").ano(2019).diaria(150.00).build();
		Veiculo veiculo2 = veiculoBuilder.placa("GLM6900").ano(2019).diaria(100.00).build();
		Veiculo veiculo3 = veiculoBuilder.placa("HJL8900").ano(2019).diaria(50.00).build();
		
		Locacao locacao1 = locacaoBuilder.umaLocacao().comVeiculos(veiculo1, veiculo2, veiculo3).quantidadeDiasLocacao(3).build();
		
		Double saidaEsperada = 900.00;
		Double saidaAtual = LocacaoBO.calcularValorTotalLocacao(locacao1.getVeiculos(), locacao1.getQuantidadeDiasLocacao());

		Assertions.assertEquals(saidaEsperada, saidaAtual);
		
	}

}
