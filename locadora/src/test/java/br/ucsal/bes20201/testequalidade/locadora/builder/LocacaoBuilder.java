package br.ucsal.bes20201.testequalidade.locadora.builder;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.rmi.CORBA.Util;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;

public class LocacaoBuilder {
			
	private static final Integer SEQ_DEFAULT = 0;
	private static final Integer NUMERO_CONTRATO_DEFAULT = 102;
	private static final Cliente CLIENTE_DEFAULT = new Cliente("051.345.897-78", "Ramon", "9090-0909");
	private static final List<Veiculo> VEICULOS_DEFAULT = new ArrayList<>();
	private static final Date DATA_LOCACAO_DEFAULT = new GregorianCalendar(2020, 1, 1).getTime();
	private static final Integer QUANT_DIAS_LOCACAO_DEFAULT = 4;
	private static final Date DATA_DEVOLUCAO_DEFAULT = new GregorianCalendar(2020, 2, 1).getTime();
	
	private Integer seq = SEQ_DEFAULT;
	private Integer numeroContrato = NUMERO_CONTRATO_DEFAULT;
	private Cliente cliente = CLIENTE_DEFAULT;
	private List<Veiculo> veiculos = VEICULOS_DEFAULT;
	private Date dataLocacao = DATA_LOCACAO_DEFAULT;
	private Integer quantidadeDias = QUANT_DIAS_LOCACAO_DEFAULT;
	private Date dataDevolucao = DATA_DEVOLUCAO_DEFAULT;
	
	
	private LocacaoBuilder() {
		
	}
	
	public static LocacaoBuilder umaLocacao() {
		return new LocacaoBuilder();
	}
	
	public LocacaoBuilder quantidadeDiasLocacao(Integer quantidadeDias) {
		this.quantidadeDias = quantidadeDias;
		return this;
	}
	
	public LocacaoBuilder comVeiculos(Veiculo...veiculos){
		this.veiculos.addAll(Arrays.asList(veiculos));
		return this;
	}

	public Locacao build() {
		return null;
	}


}
