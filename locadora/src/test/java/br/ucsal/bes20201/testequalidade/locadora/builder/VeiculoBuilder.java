package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	private static final String PLACA_DEFAULT = "PJD5890";
	private static final Integer ANO_DEFAULT = 2020;
	private static final Modelo MODELO_DEFAULT = new Modelo("Traker");
	private static final Double VALORDIARIA_DEFAULT = 120.00;
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa = PLACA_DEFAULT;
	private Integer ano = ANO_DEFAULT;
	private Modelo modelo = MODELO_DEFAULT;
	private Double valorDiaria = VALORDIARIA_DEFAULT;
	private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;
	
	private VeiculoBuilder() {
		
	}
	
	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}
	
	public VeiculoBuilder placa(String placa) {
		this.placa = placa;
		return this;
	}
	
	public VeiculoBuilder ano(Integer ano) {
		this.ano = ano;
		return this;
	}
	
	public VeiculoBuilder modelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuilder diaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}
	
	public VeiculoBuilder situacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAno(ano);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		return veiculo;
	}
}
